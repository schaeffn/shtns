/// DO NOT MODIFY DIRECTLY! file generated by cuda2hip_gen.py
/// this files simply translates cuda (nvidia) API to HIP (amd) API
#define cudaError_t	 hipError_t
#define cudaEventCreate	 hipEventCreate
#define cudaEventCreateWithFlags	 hipEventCreateWithFlags
#define cudaEventDestroy	 hipEventDestroy
#define cudaEventDisableTiming	 hipEventDisableTiming
#define cudaEventElapsedTime	 hipEventElapsedTime
#define cudaEventRecord	 hipEventRecord
#define cudaEventSynchronize	 hipEventSynchronize
#define cudaEvent_t	 hipEvent_t
#define cudaFree	 hipFree
#define cudaGetDevice	 hipGetDevice
#define cudaGetDeviceCount	 hipGetDeviceCount
#define cudaGetDeviceProperties	 hipGetDeviceProperties
#define cudaGetErrorString	 hipGetErrorString
#define cudaGetLastError	 hipGetLastError
#define cudaMalloc	 hipMalloc
#define cudaMemcpy	 hipMemcpy
#define cudaMemcpyAsync	 hipMemcpyAsync
#define cudaMemcpyDeviceToHost	 hipMemcpyDeviceToHost
#define cudaMemcpyHostToDevice	 hipMemcpyHostToDevice
#define cudaMemsetAsync	 hipMemsetAsync
#define cudaMemset2DAsync	 hipMemset2DAsync
#define cudaSetDevice	 hipSetDevice
#define cudaStreamCreateWithFlags	 hipStreamCreateWithFlags
#define cudaStreamDestroy	 hipStreamDestroy
#define cudaStreamNonBlocking	 hipStreamNonBlocking
#define cudaStreamWaitEvent	 hipStreamWaitEvent
#define cudaStream_t	 hipStream_t
#define cudaSuccess	 hipSuccess
#define cuDeviceGet	 hipDeviceGet
#define cuModuleGetFunction	 hipModuleGetFunction
#define cuModuleLoadDataEx	 hipModuleLoadDataEx
#define cufftDestroy	 hipfftDestroy
#define cufftDoubleComplex	 hipfftDoubleComplex
#define cufftExecZ2Z	 hipfftExecZ2Z
#define cufftGetSize	 hipfftGetSize
#define cufftHandle	 hipfftHandle
#define cufftPlanMany	 hipfftPlanMany
#define cufftResult	 hipfftResult
#define cufftSetStream	 hipfftSetStream
#define cufftType	 hipfftType
#define nvrtcAddNameExpression	 hiprtcAddNameExpression
#define nvrtcCompileProgram	 hiprtcCompileProgram
#define nvrtcCreateProgram	 hiprtcCreateProgram
#define nvrtcDestroyProgram	 hiprtcDestroyProgram
#define nvrtcGetErrorString	 hiprtcGetErrorString
#define nvrtcGetLoweredName	 hiprtcGetLoweredName
#define nvrtcGetProgramLog	 hiprtcGetProgramLog
#define nvrtcGetProgramLogSize	 hiprtcGetProgramLogSize
#define nvrtcProgram	 hiprtcProgram
#define nvrtcResult	 hiprtcResult
#define nvrtcGetPTX	 hiprtcGetCode
#define nvrtcGetPTXSize	 hiprtcGetCodeSize
#define cuLaunchKernel	 hipModuleLaunchKernel
#define NVRTC_SUCCESS	 HIPRTC_SUCCESS
#define CUDA_SUCCESS	 hipSuccess
#define CUdevice	 hipDevice_t
#define CUfunction	 hipFunction_t
#define CUmodule	 hipModule_t
#define CUresult	 hipError_t
#define cudaDeviceProp	 hipDeviceProp_t
#define cudaMallocHost	 hipHostMalloc
#define cudaFreeHost	 hipHostFree
#define CUFFT_D2Z	 HIPFFT_D2Z
#define CUFFT_FORWARD	 HIPFFT_FORWARD
#define CUFFT_INVERSE	 HIPFFT_BACKWARD
#define CUFFT_SUCCESS	 HIPFFT_SUCCESS
#define CUFFT_Z2Z	 HIPFFT_Z2Z
